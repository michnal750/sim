﻿using ExifLibrary;
using MaterialDesignThemes.Wpf;
using SimProject.Classes;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using static ExifLibrary.MathEx;

namespace SimProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly PhotoCatalogPage photoCatalogPage;
        private readonly MapPage mapPage;
        private readonly InformationPage informationPage;
        public SharedData sharedData;

        public MainWindow()
        {
            InitializeComponent();
            sharedData = new SharedData();
            photoCatalogPage = new PhotoCatalogPage(sharedData);
            mapPage = new MapPage(sharedData);
            informationPage = new InformationPage();
            mainGrid.Children.Add(photoCatalogPage);
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e) => DragMove();

        private void MenuListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = MenuListView.SelectedIndex;
            MoveCursorMenu(index);

            switch (index)
            {
                case 0:
                    mainGrid.Children.Clear();
                    mainGrid.Children.Add(photoCatalogPage);
                    break;
                case 1:
                    mainGrid.Children.Clear();
                    mainGrid.Children.Add(mapPage);
                    break;
                case 2:
                    mainGrid.Children.Clear();
                    mainGrid.Children.Add(informationPage);
                    break;

                default:
                    mainGrid.Children.Clear();
                    break;
            }
        }

        private void MoveCursorMenu(int index)
        {
            TransitioningContentSlide.OnApplyTemplate();
            cursorGrid.Margin = new Thickness(0, cursorGrid.ActualHeight * index + 60 + (index * 20), 0, 0);
        }

        private void MinimizeButton_Click(object sender, RoutedEventArgs e) => WindowState = WindowState.Minimized;

        private void MaximizeButton_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
                MaximizeButtonIcon.Kind = PackIconKind.WindowMaximize;
            }
            else
            {
                WindowState = WindowState.Maximized;
                MaximizeButtonIcon.Kind = PackIconKind.WindowRestore;
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            var modifiedElements = sharedData.ImageItemList.FindAll(item => item.IsModified);

            foreach (var element in modifiedElements)
            {
                UFraction32[] elementLatitude = element.GPSLatitude;
                UFraction32[] elementLongitude = element.GPSLongitude;
                var file = ImageFile.FromFile(element.Path);
                file.Properties.Set(ExifTag.GPSLatitude,
                    ConversionUtilities.ConvertUFraction32ToFloat(elementLatitude[0]),
                    ConversionUtilities.ConvertUFraction32ToFloat(elementLatitude[1]),
                    ConversionUtilities.ConvertUFraction32ToFloat(elementLatitude[2]));
                file.Properties.Set(ExifTag.GPSLatitudeRef, element.GPSLatitudeRef);
                file.Properties.Set(ExifTag.GPSLongitude,
                    ConversionUtilities.ConvertUFraction32ToFloat(elementLongitude[0]),
                    ConversionUtilities.ConvertUFraction32ToFloat(elementLongitude[1]),
                    ConversionUtilities.ConvertUFraction32ToFloat(elementLongitude[2]));
                file.Properties.Set(ExifTag.GPSLongitudeRef, element.GPSLongitudeRef);
                file.Save(element.Path);
            }

            Application.Current.Shutdown();
        }
    }
}
