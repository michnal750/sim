﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Windows.Devices.Geolocation;
using Windows.UI.Xaml.Controls.Maps;

using MapControl = Microsoft.Toolkit.Wpf.UI.Controls.MapControl;
using MapElementClickEventArgs = Microsoft.Toolkit.Win32.UI.Controls.Interop.WinRT.MapElementClickEventArgs;
using MapInputEventArgs = Microsoft.Toolkit.Win32.UI.Controls.Interop.WinRT.MapInputEventArgs;

namespace SimProject
{
    /// <summary>
    /// Interaction logic for MapPage.xaml
    /// </summary>
    public partial class MapPage : UserControl
    {
        private readonly ImagePopup imagePopup;
        private MapControl mapControl;
        private List<MapElement> mapElements;
        private List<ImageItem> imageItemList;

        private Geopoint defaultPosition;

        private int selectedMapElementIndex = 0;
        private bool editMode = false;
        private bool addMode = false;

        ObservableCollection<ImageItem> cbItems;

        public MapPage(SharedData sharedData)
        {
            InitializeComponent();
            imageItemList = sharedData.ImageItemList;
            mapElements = new List<MapElement>();
            cbItems = new ObservableCollection<ImageItem>();

            var defaultGeoposition = new BasicGeoposition() { Latitude = 51.9189, Longitude = 25.1344 };
            defaultPosition = new Geopoint(defaultGeoposition);

            mapControl = new MapControl();
            mapControl.MapServiceToken = "9uPydRQ3UmLvPebTSjYF~VSTIz1B0MVLt-sS2jBbu6A~AjQ4mtSo9RdGrBY-gk0olWSfu7RbzwH8t6DUto9_oYmp-_WK3WbPjsp23uLFBg3D";
            mapGrid.Children.Add(mapControl);
            mapControl.Loaded += MapControl_Loaded;
            mapControl.MapElementClick += MyMap_MapElementClick;
            mapControl.MapTapped += MapControl_MapTapped;

            imagePopup = new ImagePopup();
            popupGrid.Children.Add(imagePopup);
        }

        private void MapControl_MapTapped(object sender, MapInputEventArgs e)
        {
            if (editMode)
            {
                var selectedMapElement = mapElements.ElementAt(selectedMapElementIndex);
                mapElements.Remove(selectedMapElement);
                mapControl.MapElements.Remove(selectedMapElement);
                var selectedImageItem = imageItemList.Where(x => x.Path == selectedMapElement.Tag.ToString()).FirstOrDefault();
                addLandmark(e.Location, selectedImageItem.Name, selectedImageItem.Path);
                selectedImageItem.UpdateGPSLocalization(e.Location);
                selectedMapElementIndex = mapElements.Count - 1;
                refreshMap();
            }
            else if (addMode)
            {
                var selectedImageItem = imageItemList.Where(x => x.Path == (ImageWithoutLocationComboBox.SelectedItem as ImageItem).Path).FirstOrDefault();
                addLandmark(e.Location, selectedImageItem.Name, selectedImageItem.Path);
                selectedImageItem.UpdateGPSLocalization(e.Location);
                updateCheckBox();
                refreshMap();
            }
        }

        private void MyMap_MapElementClick(object sender, MapElementClickEventArgs args)
        {
            var myClickedIcon = args.MapElements.FirstOrDefault() as MapIcon;
            selectImage(myClickedIcon);
        }

        private async void MapControl_Loaded(object sender, RoutedEventArgs e)
        {
            mapControl.MapElements.Clear();
            mapElements.Clear();
            foreach (var imageItem in imageItemList)
            {
                if (imageItem.ContainsGPSLocation)
                {
                    var geoposition = new BasicGeoposition()
                    {
                        Latitude = ConversionUtilities.ConvertGPSLatitudeToFloat(imageItem.GPSLatitude, imageItem.GPSLatitudeRef),
                        Longitude = ConversionUtilities.ConvertGPSLongitudeToFloat(imageItem.GPSLongitude, imageItem.GPSLongitudeRef)
                    };
                    var geopoint = new Geopoint(geoposition);
                    addLandmark(geopoint, imageItem.Name, imageItem.Path);
                }
            }
            refreshMap();

            if (!mapElements.Any())
            {
                ChangeLocationBtn.IsEnabled = false;
                await mapControl.TrySetViewAsync(defaultPosition, 8);
            }
            else
            {
                ChangeLocationBtn.IsEnabled = true;
                selectImage(mapElements.FirstOrDefault() as MapIcon);
            }
            updateCheckBox();
        }

        private void updateCheckBox()
        {
            var imageItemsWithoutLocation = imageItemList.Where(x => !x.ContainsGPSLocation);
            cbItems = new ObservableCollection<ImageItem>();

            foreach (var imageItem in imageItemsWithoutLocation)
            {
                cbItems.Add(imageItem);
            }

            ImageWithoutLocationComboBox.ItemsSource = cbItems;

            if (cbItems.Any())
            {
                ImageWithoutLocationComboBox.SelectedItem = cbItems.First();
                ImageWithoutLocationComboBox.ItemsSource = cbItems;
                AddLocationBtn.IsEnabled = true;
            }
            else
            {
                addMode = false;
                AddLocationBtn.IsEnabled = false;
                if (mapElements.Any())
                {
                    ChangeLocationBtn.IsEnabled = true;
                }
            }
        }

        private void addLandmark(Geopoint geopoint, string title, string path)
        {
            var photoIcon = new MapIcon
            {
                Location = geopoint,
                NormalizedAnchorPoint = new Windows.Foundation.Point(1, 1),
                ZIndex = 0,
                Title = title,
                Image = Windows.Storage.Streams.RandomAccessStreamReference.CreateFromUri(new Uri(path)),
                CollisionBehaviorDesired = MapElementCollisionBehavior.RemainVisible,
                Tag = path
            };

            mapElements.Add(photoIcon);
        }

        private void refreshMap()
        {
            foreach (var mapElement in mapElements)
            {
                if (!mapControl.MapElements.Contains(mapElement))
                {
                    mapControl.MapElements.Add(mapElement);
                }
            }
        }

        private async void selectImage(MapIcon mapIcon)
        {
            selectedMapElementIndex = mapElements.IndexOf(mapIcon);

            var selectedImageItem = imageItemList.Where(x => x.Path == mapIcon.Tag.ToString()).FirstOrDefault();
            displayImage(selectedImageItem.Path, selectedImageItem.Name);

            await mapControl.TrySetViewAsync(mapIcon.Location, 8);
        }

        private async void selectImage(int newIndex)
        {
            if (newIndex < 0)
                selectedMapElementIndex = 0;
            else if (newIndex < mapElements.Count)
                selectedMapElementIndex = newIndex;

            var mapIcon = mapElements.ElementAt(selectedMapElementIndex) as MapIcon;

            var selectedImageItem = imageItemList.Where(x => x.Path == mapIcon.Tag.ToString()).FirstOrDefault();
            displayImage(selectedImageItem.Path, selectedImageItem.Name);

            await mapControl.TrySetViewAsync(mapIcon.Location, 8);
        }

        private void displayImage(string path, string name)
        {
            var bi = new System.Windows.Media.Imaging.BitmapImage();
            bi.BeginInit();
            bi.CacheOption = System.Windows.Media.Imaging.BitmapCacheOption.OnLoad;
            bi.UriSource = new Uri(path);
            bi.EndInit();
            SelectedImage.Source = bi;
            SelectedImageText.Text = name;
        }

        private void ChangeLocationBtn_Click(object sender, RoutedEventArgs e)
        {
            if (editMode)
            {
                editMode = false;
                if (cbItems.Any())
                {
                    AddLocationBtn.IsEnabled = true;
                }
                ChangeLocationBtnText.Text = "Change location";
            }
            else
            {
                editMode = true;
                AddLocationBtn.IsEnabled = false;
                ChangeLocationBtnText.Text = "End edit mode";
            }
        }

        private void NextPhotoBtn_Click(object sender, RoutedEventArgs e)
        {
            if (mapElements.Any())
            {
                selectImage(selectedMapElementIndex + 1);
            }
        }

        private void PreviousPhotoBtn_Click(object sender, RoutedEventArgs e)
        {
            if (mapElements.Any())
            {
                selectImage(selectedMapElementIndex - 1);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (addMode)
            {
                addMode = false;
                if (mapElements.Any())
                {
                    ChangeLocationBtn.IsEnabled = true;
                }
                AddLocationBtnText.Text = "Add location";
            }
            else
            {
                addMode = true;
                ChangeLocationBtn.IsEnabled = false;
                AddLocationBtnText.Text = "End add mode";
            }
        }

        private void DisplayDetailsBtn_Click(object sender, RoutedEventArgs e)
        {
            var selectedMapElement = mapElements.ElementAt(selectedMapElementIndex);
            var selectedImageItem = imageItemList.Where(x => x.Path == selectedMapElement.Tag.ToString()).FirstOrDefault();
            imagePopup.OnPhotoClick(selectedImageItem);

        }
    }
}
