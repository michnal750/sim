﻿using ExifLibrary;
using Microsoft.WindowsAPICodePack.Dialogs;
using SimProject.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace SimProject
{
    public partial class PhotoCatalogPage : UserControl
    {
        private readonly ImagePopup imagePopup;
        List<ImageItem> ImageItemList;
        List<ImageItem> InnerImageItemList;
        ImageItem selectedItem;

        public PhotoCatalogPage(SharedData sharedData)
        {
            InitializeComponent();
            ImageItemList = sharedData.ImageItemList;
            InnerImageItemList = new List<ImageItem>();
            InitializeComboboxes();
            imagePopup = new ImagePopup();
            popupGrid.Children.Add(imagePopup);
        }

        private bool hasCorrectExtension(string filepath)
        {
            var extension = Path.GetExtension(filepath);
            extension = extension.ToLower();
            return extension == ".jpg" || extension == ".png";
        }

        private void LoadPhotoButton_Click(object sender, RoutedEventArgs e)
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.IsFolderPicker = true;
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                string[] filesInDirectory = Directory.GetFiles(dialog.FileName);
                ImageItemList.Clear();
                InnerImageItemList.Clear();
                foreach (var filepath in filesInDirectory)
                {
                    if (hasCorrectExtension(filepath))
                    {
                        uint imageHeight = 0;
                        uint imageWidth = 0;
                        using (var img = System.Drawing.Image.FromFile(filepath))
                        {
                            imageHeight = (uint)img.Height;
                            imageWidth = (uint)img.Width;
                        }

                        var imageFileProperties = ImageFile.FromFile(filepath).Properties;

                        var GPSLatitudeValue = imageFileProperties.Get<GPSLatitudeLongitude>(ExifTag.GPSLatitude)?.Value;
                        var GPSLatitudeRefValue = imageFileProperties.Get<ExifEnumProperty<GPSLatitudeRef>>(ExifTag.GPSLatitudeRef)?.Value ?? GPSLatitudeRef.North;
                        var GPSLongitudeValue = imageFileProperties.Get<GPSLatitudeLongitude>(ExifTag.GPSLongitude)?.Value;
                        var GPSLongitudeRefValue = imageFileProperties.Get<ExifEnumProperty<GPSLongitudeRef>>(ExifTag.GPSLongitudeRef)?.Value ?? GPSLongitudeRef.East;

                        ImageItemList.Add(new ImageItem()
                        {
                            Name = Path.GetFileName(filepath),
                            Path = filepath,

                            Make = imageFileProperties.Get<ExifAscii>(ExifTag.Make)?.Value,
                            Orientation = imageFileProperties.Get<ExifEnumProperty<ExifLibrary.Orientation>>(ExifTag.Orientation)?.Value,
                            ShutterSpeedValue = imageFileProperties.Get<ExifSRational>(ExifTag.ShutterSpeedValue)?.Value,
                            ImageWidth = imageWidth,
                            ImageHeight = imageHeight,
                            ImageSize = imageWidth * imageHeight,
                            FocalLength = imageFileProperties.Get<ExifSRational>(ExifTag.FocalLength)?.Value,
                            DateTime = imageFileProperties.Get<ExifDateTime>(ExifTag.DateTime)?.Value,
                            HasFlashFired = imageFileProperties.Get<ExifEnumProperty<Flash>>(ExifTag.Flash)?.Value == Flash.FlashFired,
                            GPSLatitude = GPSLatitudeValue,
                            GPSLatitudeRef = GPSLatitudeRefValue,
                            GPSLongitude = GPSLongitudeValue,
                            GPSLongitudeRef = GPSLongitudeRefValue,
                            ContainsGPSLocation = GPSLatitudeValue != null && GPSLongitudeValue != null,
                            IsModified = false,
                        });
                    }
                }
                InnerImageItemList = new List<ImageItem>(ImageItemList);
                ListViewImages.ItemsSource = InnerImageItemList;
            }
        }

        private void OnPhotoClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            System.Windows.Media.Brush img = (e.Source as Border).Background;
            System.Windows.Media.ImageSource imgSource = (img as System.Windows.Media.ImageBrush).ImageSource;
            string fileName = Path.GetFileName(imgSource.ToString());
            ImageItem item = ImageItemList.Find(i => i.Name == fileName);
            selectedItem = item;
            imagePopup.OnPhotoClick(selectedItem);
        }

        private void InitializeComboboxes()
        {
            InitizalizeOrientationCombobox();
            InitizalizeImageSizeComboboxes();
            InitializeSortCombobox();
        }

        #region InitializeComboboxes
        private void InitializeSortCombobox()
        {
            Dictionary<int, string> sortComboboxSource = new Dictionary<int, string>();
            sortComboboxSource.Add(1, "Name ascending");
            sortComboboxSource.Add(2, "Name descending");
            sortComboboxSource.Add(3, "Image size ascending");
            sortComboboxSource.Add(4, "Image size descending");
            sortComboboxSource.Add(5, "Date ascending");
            sortComboboxSource.Add(6, "Date descending");
            sortCombobox.ItemsSource = sortComboboxSource;
            sortCombobox.DisplayMemberPath = "Value";
            sortCombobox.Items.Refresh();
        }

        private void InitizalizeImageSizeComboboxes()
        {
            Dictionary<int, string> SizeComboSource = new Dictionary<int, string>();
            SizeComboSource.Add(1, "Small");
            SizeComboSource.Add(2, "Medium");
            SizeComboSource.Add(3, "Big");
            heightCombobox.ItemsSource = SizeComboSource;
            heightCombobox.DisplayMemberPath = "Value";
            heightCombobox.Items.Refresh();
            widthCombobox.ItemsSource = SizeComboSource;
            widthCombobox.DisplayMemberPath = "Value";
            widthCombobox.Items.Refresh();
        }

        private void InitizalizeOrientationCombobox()
        {
            Dictionary<int, string> orientationComboSource = new Dictionary<int, string>();
            orientationComboSource.Add((int)ExifLibrary.Orientation.Normal, "Normal");
            orientationComboSource.Add((int)ExifLibrary.Orientation.Flipped, "Flipped");
            orientationComboSource.Add((int)ExifLibrary.Orientation.Rotated180, "Rotated 180");
            orientationComboSource.Add((int)ExifLibrary.Orientation.FlippedAndRotated180, "Flipped And Rotated180");
            orientationComboSource.Add((int)ExifLibrary.Orientation.FlippedAndRotatedLeft, "Flipped And RotatedLeft");
            orientationComboSource.Add((int)ExifLibrary.Orientation.RotatedLeft, "Rotated Left");
            orientationComboSource.Add((int)ExifLibrary.Orientation.FlippedAndRotatedRight, "Flipped And Rotated Right");
            orientationComboSource.Add((int)ExifLibrary.Orientation.RotatedRight, "Rotated Right");
            orientationCombobox.ItemsSource = orientationComboSource;
            orientationCombobox.DisplayMemberPath = "Value";
            orientationCombobox.Items.Refresh();
        }
        #endregion

        private void sortSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ImageItemList.Any())
            {
                var selectedObject = sortCombobox.SelectedItem;
                string value = selectedObject.GetType().GetProperty("Value").GetValue(selectedObject, null).ToString();

                bool isAcending = value.Split().Last() == "ascending";
                string propertyName = value.Substring(0, value.LastIndexOf(" ")).Trim();

                List<ImageItem> sortedImages = null;
                if (propertyName == "Name")
                {
                    sortedImages = (isAcending ? InnerImageItemList.OrderBy(item => item.Name) : InnerImageItemList.OrderByDescending(item => item.Name)).ToList();
                }
                else if (propertyName == "Image size")
                {
                    sortedImages = (isAcending ? InnerImageItemList.OrderBy(item => item.ImageSize) : InnerImageItemList.OrderByDescending(item => item.ImageSize)).ToList();
                }
                else if (propertyName == "Date")
                {
                    sortedImages = (isAcending ? InnerImageItemList.OrderBy(item => item.DateTime) : InnerImageItemList.OrderByDescending(item => item.DateTime)).ToList();
                }

                ListViewImages.ItemsSource = sortedImages;
            }
        }

        private void filterButton_Click(object sender, RoutedEventArgs e)
        {
            FilterPanel filter = null;
            try
            {
                filter = getDataFromPanelFilter();
            }
            catch
            {
                MessageBox.Show("Incorrect data in filter panel!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            IEnumerable<ImageItem> query = ImageItemList;
            if (!string.IsNullOrEmpty(filter.PhotoName))
                query = query.Where(x => x.Name.ToLower().Contains(filter.PhotoName.ToLower()));
            if (!string.IsNullOrEmpty(filter.CameraType))
                query = query.Where(x => !string.IsNullOrEmpty(x.Make) ? x.Make.Contains(filter.CameraType) : false);
            if (filter.ContainsGPSLocation.HasValue)
                query = query.Where(x => x.ContainsGPSLocation == filter.ContainsGPSLocation);
            if (filter.HasFlashFired.HasValue)
                query = query.Where(x => x.HasFlashFired == filter.HasFlashFired);
            if (filter.DateTimeFrom.HasValue)
                query = query.Where(x => x.DateTime != null ? x.DateTime >= filter.DateTimeFrom : false);
            if (filter.DateTimeTo.HasValue)
                query = query.Where(x => x.DateTime != null ? x.DateTime <= filter.DateTimeTo : false);
            if (filter.Orientation.HasValue)
                query = query.Where(x => x.Orientation.HasValue ? (int)x.Orientation.Value == filter.Orientation.Value : false);
            if (filter.ShutterSpeedFrom.HasValue)
                query = query.Where(x => x.ShutterSpeedValue.HasValue ? x.ShutterSpeedValue.Value > filter.ShutterSpeedFrom : false);
            if (filter.ShutterSpeedTo.HasValue)
                query = query.Where(x => x.ShutterSpeedValue.HasValue ? x.ShutterSpeedValue.Value < filter.ShutterSpeedTo : false);
            if (filter.FocalLengthFrom.HasValue)
                query = query.Where(x => x.FocalLength.HasValue ? x.FocalLength.Value > filter.FocalLengthFrom : false);
            if (filter.FocalLengthTo.HasValue)
                query = query.Where(x => x.FocalLength.HasValue ? x.FocalLength.Value < filter.FocalLengthTo : false);
            if (filter.ImageWidth.HasValue)
                query = query.Where(x => x.ImageWidth.HasValue ? checkSize(x.ImageWidth.Value, filter.ImageWidth.Value) : false);
            if (filter.ImageHeight.HasValue)
                query = query.Where(x => x.ImageHeight.HasValue ? checkSize(x.ImageHeight.Value, filter.ImageHeight.Value) : false);
            InnerImageItemList = query.ToList();
            ListViewImages.ItemsSource = InnerImageItemList;
        }

        private bool checkSize(uint size, int sizeEnum)
        {
            if (sizeEnum == 1)
            {
                return size < 100;
            }
            else if (sizeEnum == 2)
            {
                return size < 500;
            }
            else if (sizeEnum == 3)
            {
                return true;
            }
            return false;
        }

        private FilterPanel getDataFromPanelFilter()
        {
            try
            {
                FilterPanel filterPanel = new FilterPanel();
                filterPanel.CameraType = CameraTextBox.Text;
                if (orientationCombobox.SelectedItem != null)
                    filterPanel.Orientation = ((KeyValuePair<int, string>)orientationCombobox.SelectedItem).Key;
                if (!string.IsNullOrEmpty(shutterFromTextBox.Text))
                    filterPanel.ShutterSpeedFrom = new MathEx.Fraction32(shutterFromTextBox.Text);
                if (!string.IsNullOrEmpty(shutterToTextBox.Text))
                    filterPanel.ShutterSpeedTo = new MathEx.Fraction32(shutterToTextBox.Text);
                if (widthCombobox.SelectedItem != null)
                    filterPanel.ImageWidth = ((KeyValuePair<int, string>)widthCombobox.SelectedItem).Key;
                if (heightCombobox.SelectedItem != null)
                    filterPanel.ImageHeight = ((KeyValuePair<int, string>)heightCombobox.SelectedItem).Key;
                if (!string.IsNullOrEmpty(FocalFromTextBox.Text))
                    filterPanel.FocalLengthFrom = new MathEx.Fraction32(FocalFromTextBox.Text);
                if (!string.IsNullOrEmpty(FocalToTextBox.Text))
                    filterPanel.FocalLengthTo = new MathEx.Fraction32(FocalToTextBox.Text);
                filterPanel.DateTimeFrom = DateFrom.SelectedDate;
                filterPanel.DateTimeTo = DateTo.SelectedDate;
                filterPanel.HasFlashFired = FlashUsed.IsChecked;
                filterPanel.ContainsGPSLocation = ContainsGPS.IsChecked;
                filterPanel.PhotoName = searchBox.Text;
                return filterPanel;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CameraTextBox.Text = "";
            orientationCombobox.SelectedIndex = -1;
            shutterFromTextBox.Text = "";
            shutterToTextBox.Text = "";
            widthCombobox.SelectedIndex = -1;
            heightCombobox.SelectedIndex = -1;
            FocalFromTextBox.Text = "";
            FocalToTextBox.Text = "";
            DateFrom.SelectedDate = null;
            DateTo.SelectedDate = null;
            FlashUsed.IsChecked = null;
            ContainsGPS.IsChecked = null;
            searchBox.Text = "";

        }
    }
}


