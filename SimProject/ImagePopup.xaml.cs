﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace SimProject
{
    /// <summary>
    /// Interaction logic for Popup.xaml
    /// </summary>
    public partial class ImagePopup : UserControl
    {
        ImageItem selectedItem;
        double maxScale = 0.60;
        public ImagePopup()
        {
            InitializeComponent();
        }

        public void OnPhotoClick(ImageItem selectedItem)
        {
            PhotoDisplayPopup.IsOpen = true;

            displayImage(selectedItem.Path);

            var maxWidth = maxScale * SystemParameters.PrimaryScreenWidth;
            var maxHeight = maxScale * SystemParameters.PrimaryScreenHeight;
            double imageWidth = selectedItem.ImageWidth ?? 0.0;
            double imageHeight = selectedItem.ImageHeight ?? 0.0;

            if (imageWidth > maxWidth || imageHeight > maxHeight)
            {
                double scaleWidth = 1.0;
                double scaleHeight = 1.0;
                if (imageWidth > maxWidth)
                {
                    scaleWidth = maxWidth / imageWidth;
                }
                if (imageHeight > maxHeight)
                {
                    scaleHeight = maxHeight / imageHeight;
                }
                double scale = scaleWidth <= scaleHeight ? scaleWidth : scaleHeight;
                PhotoDisplay.Width = imageWidth * scale;
                PhotoDisplay.Height = imageHeight * scale;
            }
            else
            {
                PhotoDisplay.Width = imageWidth;
                PhotoDisplay.Height = imageHeight;
            }
            this.selectedItem = selectedItem;
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            PhotoDisplayPopup.IsOpen = false;
            selectedItem = null;

            if (PhotoInfoPopup.IsExpanded)
            {
                PhotoInfoPopup.IsExpanded = false;
            }
        }

        private void displayImage(string path)
        {
            var bi = new System.Windows.Media.Imaging.BitmapImage();
            bi.BeginInit();
            bi.CacheOption = System.Windows.Media.Imaging.BitmapCacheOption.OnLoad;
            bi.UriSource = new Uri(path);
            bi.EndInit();
            PhotoDisplay.Source = bi;
        }

        private void PhotoInfoPopup_Expanded(object sender, RoutedEventArgs e)
        {
            if (selectedItem != null)
            {
                PhotoInfoName.Inlines.Add(" " + selectedItem.Name);
                PhotoInfoPath.Inlines.Add(" " + selectedItem.Path);
                PhotoInfoMake.Inlines.Add(" " + selectedItem.Make);
                PhotoInfoOrientation.Inlines.Add(" " + selectedItem.Orientation?.ToString());
                PhotoInfoShutterSpeed.Inlines.Add(" " + selectedItem.ShutterSpeedValue?.ToString());
                PhotoInfoWidth.Inlines.Add(" " + selectedItem.ImageWidth?.ToString());
                PhotoInfoHeight.Inlines.Add(" " + selectedItem.ImageHeight?.ToString());
                PhotoInfoFocalLength.Inlines.Add(" " + selectedItem.FocalLength?.ToString());
                PhotoInfoDateTime.Inlines.Add(" " + selectedItem.DateTime?.ToString());
                PhotoInfoHasFlashFired.Inlines.Add(" " + selectedItem.HasFlashFired.ToString());
                string latitude = " ";
                string longitude = " ";
                if (selectedItem.GPSLatitude != null)
                {
                    latitude += ConversionUtilities.ConvertGPSLatitudeToFloat(selectedItem.GPSLatitude, selectedItem.GPSLatitudeRef).ToString();
                }
                if (selectedItem.GPSLongitude != null)
                {
                    longitude += ConversionUtilities.ConvertGPSLongitudeToFloat(selectedItem.GPSLongitude, selectedItem.GPSLongitudeRef).ToString();
                }

                PhotoInfoGpsLatitude.Inlines.Add(latitude);
                PhotoInfoGpsLongitude.Inlines.Add(longitude);
                PhotoInfoContainsGpsLocation.Inlines.Add(" " + selectedItem.ContainsGPSLocation.ToString());
            }
        }

        private void PhotoInfoPopup_Collapsed(object sender, RoutedEventArgs e)
        {
            PhotoInfoName.Inlines.Remove(PhotoInfoName.Inlines.LastInline);
            PhotoInfoPath.Inlines.Remove(PhotoInfoPath.Inlines.LastInline);
            PhotoInfoMake.Inlines.Remove(PhotoInfoMake.Inlines.LastInline);
            PhotoInfoOrientation.Inlines.Remove(PhotoInfoOrientation.Inlines.LastInline);
            PhotoInfoShutterSpeed.Inlines.Remove(PhotoInfoShutterSpeed.Inlines.LastInline);
            PhotoInfoWidth.Inlines.Remove(PhotoInfoWidth.Inlines.LastInline);
            PhotoInfoHeight.Inlines.Remove(PhotoInfoHeight.Inlines.LastInline);
            PhotoInfoFocalLength.Inlines.Remove(PhotoInfoFocalLength.Inlines.LastInline);
            PhotoInfoDateTime.Inlines.Remove(PhotoInfoDateTime.Inlines.LastInline);
            PhotoInfoHasFlashFired.Inlines.Remove(PhotoInfoHasFlashFired.Inlines.LastInline);
            PhotoInfoGpsLatitude.Inlines.Remove(PhotoInfoGpsLatitude.Inlines.LastInline);
            PhotoInfoGpsLongitude.Inlines.Remove(PhotoInfoGpsLongitude.Inlines.LastInline);
            PhotoInfoContainsGpsLocation.Inlines.Remove(PhotoInfoContainsGpsLocation.Inlines.LastInline);
        }
    }
}
