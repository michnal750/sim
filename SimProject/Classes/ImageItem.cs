﻿using ExifLibrary;
using SimProject.Classes;
using System;
using Windows.Devices.Geolocation;

namespace SimProject
{
    public class ImageItem
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string Make { get; set; }
        public Orientation? Orientation { get; set; }
        public MathEx.Fraction32? ShutterSpeedValue { get; set; }
        public uint? ImageWidth { get; set; }
        public uint? ImageHeight { get; set; }
        public ulong? ImageSize { get; set; }
        public MathEx.Fraction32? FocalLength { get; set; }
        public DateTime? DateTime { get; set; }
        public bool HasFlashFired { get; set; }
        public MathEx.UFraction32[] GPSLatitude { get; set; }
        public GPSLatitudeRef GPSLatitudeRef { get; set; }
        public MathEx.UFraction32[] GPSLongitude { get; set; }
        public GPSLongitudeRef GPSLongitudeRef { get; set; }
        public bool ContainsGPSLocation { get; set; }
        public bool IsModified { get; set; }

        public void UpdateGPSLocalization(Geopoint geopoint)
        {
            var latitude = geopoint.Position.Latitude;
            GPSLatitudeRef = GPSLatitudeRef.North;
            if (latitude < 0)
            {
                GPSLatitudeRef = GPSLatitudeRef.South;
            } 
            GPSLatitude = ConversionUtilities.ConvertDoubleToDMS(Math.Abs(latitude));

            var longitude = geopoint.Position.Longitude;
            GPSLongitudeRef = GPSLongitudeRef.East;
            if (longitude < 0)
            {
                GPSLongitudeRef = GPSLongitudeRef.West;
            }
            GPSLongitude = ConversionUtilities.ConvertDoubleToDMS(Math.Abs(longitude));
            IsModified = true;
            ContainsGPSLocation = true;
        }
    }
}

