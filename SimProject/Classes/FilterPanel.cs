﻿using ExifLibrary;
using System;

namespace SimProject.Classes
{
    public class FilterPanel
    {
        public string CameraType { get; set; }
        public int? Orientation { get; set; }
        public MathEx.Fraction32? ShutterSpeedFrom { get; set; }
        public MathEx.Fraction32? ShutterSpeedTo { get; set; }
        public int? ImageWidth { get; set; }
        public int? ImageHeight { get; set; }
        public MathEx.Fraction32? FocalLengthFrom { get; set; }
        public MathEx.Fraction32? FocalLengthTo { get; set; }
        public DateTime? DateTimeFrom { get; set; }
        public DateTime? DateTimeTo { get; set; }
        public bool? HasFlashFired { get; set; }
        public bool? ContainsGPSLocation { get; set; }
        public string PhotoName { get; set; }
    }
}
