﻿using ExifLibrary;
using System;
using static ExifLibrary.MathEx;

namespace SimProject
{
    static class ConversionUtilities
    {
        public static UFraction32[] ConvertDoubleToDMS(double decimalDegrees)
        {
            var degrees = decimalDegrees - Math.Floor(decimalDegrees);
            var minutes = (decimalDegrees - degrees) * 60;
            var seconds = (minutes - Math.Floor(minutes)) * 60;

            UFraction32[] results = {
                new UFraction32(degrees),
                new UFraction32(minutes),
                new UFraction32(seconds)
            };
            return results;
        }

        public static float ConvertUFraction32ToFloat(UFraction32 fraction32)
        {
            return Convert.ToSingle(fraction32.Numerator)
                / Convert.ToSingle(fraction32.Denominator);
        }

        public static float ConvertGPSLatitudeToFloat(UFraction32[] gpsCoordinate, GPSLatitudeRef latitudeRef)
        {
            float latitudeFloat = ConvertGPSCoordinateToFloat(gpsCoordinate);
            if (latitudeRef == GPSLatitudeRef.South)
            {
                return -latitudeFloat;
            }
            return latitudeFloat;
        }

        public static float ConvertGPSLongitudeToFloat(UFraction32[] gpsCoordinate, GPSLongitudeRef longitudeRef)
        {
            float longitudeFloat = ConvertGPSCoordinateToFloat(gpsCoordinate);
            if (longitudeRef == GPSLongitudeRef.West)
            {
                return -longitudeFloat;
            }
            return longitudeFloat;
        }

        private static float ConvertGPSCoordinateToFloat(UFraction32[] uFractions32)
        {
            var degrees = ConvertUFraction32ToFloat(uFractions32[0]);
            var minutes = ConvertUFraction32ToFloat(uFractions32[1]);
            var seconds = ConvertUFraction32ToFloat(uFractions32[2]);
            return degrees + (minutes / 60) + (seconds / 3600);
        }
    }
}
